[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/datadriveneducation%2Flanguage-learning/master)

# Identifying learner groups for learner recommendations

The purpose of this project is to identify different learner groups by analysing language learning data (e.g. question responses and performance in assessments). 

Details of the first dataset we are working with can be found on its [OSF page](https://osf.io/pyb8s/wiki/home/). 

Some analyses have already been conducted and reported in [Hartshorne, Tenenbaum, & Pinker. A Critical Period for Second Language Acquisition: Evidence from 2/3 Million English Speakers](https://www.sciencedirect.com/science/article/pii/S0010027718300994). This was a high profile study focusing on the time criticality of second language acquisition. You can find a summary of the study together with links to media coverage [here](https://blog.gameswithwords.org/a-critical-period-for-second-language-acquisition-evidence-from-2-3-million-english-speakers-12fca68ebc23). 

We use the data to prototype how to identify different learner groups (e.g. native/non-native, different first languages) and the similarities and differences between them.

## Setup

To get started, first clone this repository and cd into the project directory

``` 
$ git clone https://gitlab.com/datadriveneducation/language-learning.git
$ cd language-learning
```

Then install the package requirements

```
$ pip install -r requirements.txt
```

If you want to use the classic Jupyter Notebook, launch it

```
$ jupyter notebook
```

If you want to use JupyterLab, follow the instructions in the plotly.py README to install the jupyterlab python package along with the required JupyterLab JavaScript packages. Pay close attension to all of the version numbers!
